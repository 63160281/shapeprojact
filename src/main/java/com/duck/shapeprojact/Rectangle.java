/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.shapeprojact;

/**
 *
 * @author Administrator
 */
public class Rectangle {
   private double h ;
   private double w ;
   public Rectangle(double h, double w){
       this.h = h;
       this.w = w;
   }
   public double calArea(){
       return h*w;
   }
   public double getH(){
       return h;
   }
   public double getW(){
       return w;
   }
   public void setHW(double h,double w){
       if(h<=0 || w <=0){
           System.out.println("Error High and wide must more than 0!!!!");
           return;
       }
       this.h = h;
       this.w = w;
   }
}
